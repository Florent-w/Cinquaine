<?php
class Connexion
{
	private static $hostname = '172.17.0.3';
	private static $database = 'cinquaine';
	private static $login = 'root';
	private static $pwd = 'votre_mot_de_passe';
	private static $port = '3306';

	private static $tabUTF8 = array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8');

	protected static $pdo = null;

	public static function connect(): bool
	{
		try {
			$dsn = "mysql:host=" . self::$hostname . ";port=" . self::$port . ";dbname=" . self::$database;
			self::$pdo = new PDO($dsn, self::$login, self::$pwd, self::$tabUTF8);
			self::$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

			return true;
		} catch (PDOException $e) {
			echo "erreur de connexion : " . $e->getMessage() . "<br>";
			return false;
		}
	}

	public static function pdo()
	{
		return self::$pdo;
	}
}
