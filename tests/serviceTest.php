<?php

require_once "model/serviceModel.php";

use PHPUnit\Framework\TestCase;

class serviceTest extends TestCase
{
    private $service;

    protected function setUp(): void
    {
        $this->service = new Service();
    }

    public function testGetId()
    {
        $this->service->setId(1);
        $this->assertEquals(1, $this->service->getId());
    }

    public function testGetDateStart()
    {
        $date = new DateTime();
        $this->service->setDateStart($date);
        $this->assertEquals($date, $this->service->getDateStart());
    }

    // ... Other getter tests here ...

    public function testAddService()
    {
        $result = Service::addService(1, 100, 1, "Test service", "Test description");
        $this->assertTrue($result);
    }

    // ... Other static function tests here ...

    protected function tearDown(): void
    {
        $this->service = NULL;
    }
}
