<?php

require_once "model/userModel.php";

use PHPUnit\Framework\TestCase;

class userTest extends TestCase
{   
    public function testIncreaseBalance()
    {
        $userId = 1;
        $initialBalance = User::getUserById($userId)->getBalance();
        $amount = 50;
        
        User::increaseBalance($userId, $amount);
        
        $updatedBalance = User::getUserById($userId)->getBalance();
        
        $this->assertEquals($initialBalance + $amount, $updatedBalance);
    }
    
    public function testDecreaseBalance()
    {
        $userId = 1;
        $initialBalance = User::getUserById($userId)->getBalance();
        $amount = 20;
        
        User::decreaseBalance($userId, $amount);
        
        $updatedBalance = User::getUserById($userId)->getBalance();
        
        $this->assertEquals($initialBalance - $amount, $updatedBalance);
    }

    public function testAddUser() {
        // Création d'un utilisateur factice pour le test
        $name = 'John Doe';
        $password = 'password123';
        $email = 'john@example.com';
        $phone_number = '1234567890';
        $balance = 0;

        // Appel de la méthode addUser pour ajouter l'utilisateur
        $result = User::addUser($name, $password, $email, $phone_number, $balance);

        // Vérification du résultat
        $this->assertTrue($result);

        // Vérification que l'utilisateur a été ajouté en vérifiant les données dans la base de données
        $addedUser = User::getUserByUsername($name);
        $this->assertInstanceOf(User::class, $addedUser);
        $this->assertEquals($name, $addedUser->getName());
        $this->assertEquals($email, $addedUser->getEmail());
        $this->assertEquals($phone_number, $addedUser->getTelephoneNumber());
        $this->assertEquals($balance, $addedUser->getBalance());

        // Suppression de l'utilisateur ajouté pour nettoyer la base de données
        User::deleteUserById($addedUser->getId());
    }
}
